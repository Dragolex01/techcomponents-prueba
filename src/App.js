import { useState } from 'react';
import './css/App.scss';
import Header from './componentes/Header';
import Menu from './componentes/Menu';
import MenuLateral from './componentes/MenuLateral';
import Lista from './componentes/Lista';
import Footer from './componentes/Footer';
import React from 'react';

function App() {

  const [stateMenuLateral, setStateMenuLateral] = useState('oculto');

  function cambiarMenu(){
    stateMenuLateral === 'oculto'
    ? setStateMenuLateral('visible')
    : setStateMenuLateral('oculto')
  }

  return (
    <div>
      <Header />
      <Menu />
      <hr/>
      {/* <button onClick={cambiarMenu} className="menuLateral">Menú</button> */}
      <section className="seccionPrincipal">
        {/* <MenuLateral stateMenuLateral={stateMenuLateral} /> */}
        <Lista />
      </section>
      <hr />
      <Footer />
    </div>
  );
}

export default App;
