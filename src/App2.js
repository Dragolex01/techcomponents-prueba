import './css/App.scss';
import { Link, Outlet } from 'react-router-dom';
import imgMovil from './img/movil.webp';
import imgTablet from './img/tablet.webp';
import imgPortatil from './img/portatil.png';

function App(){
    return(
        <div>
            <section className='header'>
                <div className='header__contenedorTitulo'>
                    <h1>TechComponents</h1>
                    <h2>Mejores tablets, móviles y portátiles a calidad/precio</h2>
                    <Link to="/App">Prueba</Link>
                </div>
            </section>
            <section className='seccionInfo'>Información sobre nosotros</section>
            <section className='seccionProductos'>
                <h2>Nuestros productos</h2>
                <div className='seccionProductos__contenedorIzq'>
                    <img src={imgMovil} alt='imagen movil' />
                    <div className='seccionProductos__contenedorIzq__contenedorTexto'>
                        <h3>Móviles</h3>
                        <p>Sit fugiat Lorem ex dolor culpa.</p>
                        <button href="#">Ver móviles</button>
                    </div>
                </div>
                <div className='seccionProductos__contenedorDer'>
                    <div className='seccionProductos__contenedorDer__contenedorTexto'>
                        <h3>Portátiles</h3>
                        <p>Sit fugiat Lorem ex dolor culpa.</p>
                        <button href="#">Ver portátiles</button>
                    </div>
                    <img src={imgPortatil} alt='imagen portátil' />
                </div>
                <div className='seccionProductos__contenedorIzq'>
                    <img src={imgTablet} alt='imagen tablet' className='seccionProductos--img' />
                    <div className='seccionProductos__contenedorIzq__contenedorTexto'>
                        <h3>Tablets</h3>
                        <p>Sit fugiat Lorem ex dolor culpa.</p>
                        <button href="#">Ver tablets</button>
                    </div>
                </div>
            </section>
            
            <section className='footer'>Footer</section>
        </div>
    )
}

export default App;