function Footer(){
    return (
        <section className="footer">
            <div className="footer__contenedor">
                <h2>Contactanos</h2>
                <p>Email: techcomponents@gmail.com</p>
                <p>Tlf: 965 45 45 45</p>
            </div>
            <div className="footer__contenedor">
                <h2>Redes sociales</h2>
                <p>Facebook</p>
                <p>Instagram</p>
                <p>Twitter</p>
            </div>
            <div className="footer__contenedor">
                <h2>Quienes somos</h2>
                <p>Officia ipsum aliquip ad in culpa officia. Exercitation veniam ea esse minim nostrud deserunt pariatur. In nostrud esse officia in do id ad cupidatat pariatur in anim laborum. Ad cillum dolore ea incididunt labore nulla adipisicing dolore esse aliquip nulla exercitation est incididunt. Consequat non Lorem reprehenderit proident ut aliquip amet cupidatat voluptate. Pariatur non occaecat excepteur ut tempor minim est cillum in officia sunt mollit.</p>
            </div>
        </section>
    )
}

export default Footer;
