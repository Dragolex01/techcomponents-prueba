import imgLogo from '../img/logo.png';

function Header() {
    return (
        <section className="header">
            <div className="header__contenedorLogo">
                {/* <p>TechComponents</p> */}
                <img src={imgLogo} className="header__contenedorLogo--imgLogo" alt="logo" />
            </div>
            <div className="header__contenedorBuscador">
                <nav className="header__contenedorBuscador--filtro">
                    <ul>
                        <li><button type="button" className="header__contenedorBuscador--filtro-boton">Categorías</button>
                            <ul>
                                <li><a href="/">Filtro</a></li>
                                <li><a href="/">Filtro</a></li>
                                <li><a href="/">Filtro</a></li>
                            </ul>
                        </li>
                    </ul>
                </nav>
                <input type="text" className="header__contenedorBuscador--buscador" placeholder="Empieza tu busqueda..." />
                <button className="header__contenedorBuscador--boton">Buscar</button>
            </div>
            <nav className="header__contenedorLinks">
                <ul>
                    <li><a href="/">Carrito</a></li>
                    <li><a href="/">Mi cuenta</a></li>
                </ul>
            </nav>
        </section>
    )
}

export default Header;