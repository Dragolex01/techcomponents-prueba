function Item({ title, image, info, price }){
    return (
        <div className="contenedorItem">
            <h2 className="contenedorItem--title">{title}</h2>
            <img className="contenedorItem--img" src={image} alt={title} />
            <p className="contenedorItem--info">{info}</p>
            <p className="contenedorItem--price">{price}</p>
        </div>
    )
}

export default Item;