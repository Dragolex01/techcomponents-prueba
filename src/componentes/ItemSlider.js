function ItemSlider({ title, image, price }){

    return(
        <div className="contenedorItemSlider">
            <h2 className="contenedorItemSlider--title">{title}</h2>
            <img className="contenedorItemSlider--img" src={image} alt={title} />
            <p className="contenedorItemSlider--price">{price}</p>
        </div>
    )
}

export default ItemSlider;