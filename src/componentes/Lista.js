import { useEffect, useState } from 'react';
import getAsyncAPIList from '../utils/APIList.js';
import Item from './Item.js';
import Slider from './Slider.js';

function Lista(){

    const [lista, setLista] = useState([]);

    useEffect(() => {
        getAsyncAPIList().then(({list}) => {
            setLista(list);
        })
    }, [])

    return(
        <div className="contenedorLista">
            <Slider lista={lista} title="NOVEDADES" />
            <hr />
            <div className="contenedorListaItems">
                <ul className="contenedorListaItems--ul">
                    {
                        lista.map(item => {
                            return(
                                <li className="contenedorListaItems--li" key={item.objectID}>
                                    <Item title={item.title} image={item.image} info={item.info} price={item.price} />
                                </li>
                            )
                        })
                    }
                </ul>
            </div>
        </div>
    )
}

export default Lista;