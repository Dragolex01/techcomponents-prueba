function Menu(){
    return (
        <nav className="contenedorMenu">
            <ul>
                <li><a href="/">Inicio</a></li>
                <li><a href="/">Ordenadores</a></li>
                <li><a href="/">Móviles</a></li>
                <li><a href="/">Tablets</a></li>
            </ul>
        </nav>
    )
}

export default Menu;