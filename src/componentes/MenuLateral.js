function MenuLateral({ stateMenuLateral }){

    return (
        <div className={`contenedorMenuLateral ${stateMenuLateral}`}>
            MENU
        </div>
    )
}

export default MenuLateral;