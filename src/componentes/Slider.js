import ItemSlider from "./ItemSlider";

function Slider({ lista, title }){
    
    return (
        <div className="contenedorSlider">
            <h2 className="contenedorSlider--title">{title}</h2>
            <ul className="contenedorSlider--ul">
                {
                    lista.map(item => {
                        return(
                            <li className="contenedorSlider--li" key={item.objectID}>
                                <ItemSlider title={item.title} image={item.image} price={item.price} />
                            </li>
                        )
                    })
                }
            </ul>
        </div>
    )
}

export default Slider;