import React from "react";
import ReactDOM from "react-dom/client";
import { BrowserRouter, Routes, Route } from 'react-router-dom';
import App from "./App";
import App2 from "./App2";

const root = ReactDOM.createRoot(document.getElementById("root"));
root.render(
    <BrowserRouter>
      <Routes>
        <Route path='/' element={<App2 />} >
          <Route path='App2' element={<App2 />} />
          <Route path='App' element={<App />} />
        </Route>
      </Routes>
    </BrowserRouter>,
);