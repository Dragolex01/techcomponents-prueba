const APIList= [
    {
        objectID: 0,
        title: 'Samsung Galaxy A53 5G 256GB',
        image: '../img/imgPrueba.png',
        info: 'info',
        price: '509€',
    },
    {
        objectID: 1,
        title: 'Samsung Galaxy A53 5G 256GB',
        image: '../img/imgPrueba.png',
        info: 'info',
        price: '509€',
    },
    {
        objectID: 2,
        title: 'Samsung Galaxy A53 5G 256GB',
        image: '../img/imgPrueba.png',
        info: 'info',
        price: '509€',
    },
    {
        objectID: 3,
        title: 'Samsung Galaxy A53 5G 256GB',
        image: '../img/imgPrueba.png',
        info: 'info',
        price: '509€',
    },
    {
        objectID: 4,
        title: 'Samsung Galaxy A53 5G 256GB',
        image: '../img/imgPrueba.png',
        info: 'info',
        price: '509€',
    },
    {
        objectID: 5,
        title: 'Samsung Galaxy A53 5G 256GB',
        image: '../img/imgPrueba.png',
        info: 'info',
        price: '509€',
    },
    {
        objectID: 6,
        title: 'Samsung Galaxy A53 5G 256GB',
        image: '../img/imgPrueba.png',
        info: 'info',
        price: '509€',
    }
]

const getAsyncAPIList = () => Promise.resolve({list: APIList});

export default getAsyncAPIList;